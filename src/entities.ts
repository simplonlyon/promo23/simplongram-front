export interface Picture {
    id?:number;
    description?:string;
    src:string;
    publicationDate?:string|Date;
    link?:string;
    thumbnailLink?:string;
    owner?:User;
    likes?:User[];
}

export interface User {
    id?:number;
    email:string;
    password?:string;
    profilePicture?:string;
    roles: string[];
    avatar?:string;
    avatarThumbnail?:string;
}