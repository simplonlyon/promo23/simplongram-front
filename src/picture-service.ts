import axios from "axios";
import { Picture } from "./entities";


export async function fetchAllPictures() {
    const response = await axios.get<Picture[]>('/api/picture');
    return response.data;
}


export async function postPicture(picture: Picture) {
    const response = await axios.post<Picture>('/api/picture', picture);
    return response.data;
}



export async function likePicture(picture: Picture) {
    const response = await axios.patch<Picture>('/api/picture/'+picture.id+'/like');
    return response.data;
}