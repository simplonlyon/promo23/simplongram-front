import { Picture } from "@/entities";
import { postPicture } from "@/picture-service";
import { Box, Button, Step, StepButton, Stepper, TextField } from "@mui/material";
import { useRouter } from "next/router";
import { useState } from "react"
import ListItemPicture from "./ListItemPicture";


const steps = ['Choose an image', 'Enter description', 'Summary'];

export default function FormPicture() {
    const [step, setStep] = useState(0);
    const router = useRouter();

    const [picture, setPicture] = useState<Picture>({
        src: '',
        description: ''
    });

    function handleChange(event: any) {
        setPicture({
            ...picture,
            [event.target.name]: event.target.value
        });
    }
    /**
     * On récupère l'image dans l'input type file et on convertit celle ci
     * en base64 pour l'envoyer vers le back
     */
    function handleFile(event: any) {
        const reader = new FileReader();
        reader.onload = () => setPicture({
            ...picture,
            src: String(reader.result)
        });

        reader.readAsDataURL(event.target.files[0]);
    }

    async function handleSubmit() {

        await postPicture(picture);
        router.push('/');
    }

    return (
        <>
            <Stepper activeStep={step}>
                {steps.map((label, index) => (
                    <Step key={label} >
                        <StepButton color="inherit" onClick={() => setStep(index)}>
                            {label}
                        </StepButton>
                    </Step>
                ))}
            </Stepper>
            <Box sx={{ padding: '20px' }}>
                {step == 0 &&
                    <>
                        {picture.src &&
                            <div>
                                <img src={picture.src} style={{ height: 400 }} />
                            </div>
                        }
                        <Button
                            variant="contained"
                            component="label"
                        >
                            Upload File
                            <input
                                type="file"
                                hidden
                                name="src"
                                onChange={handleFile}
                            />
                        </Button>
                    </>
                }
                {step == 1 &&


                    <TextField
                        label="Description"
                        multiline
                        maxRows={4}
                        name="description"
                        value={picture.description}
                        onChange={handleChange}
                    />
                }
                {step == 2 &&
                    <>
                        <ListItemPicture picture={{ ...picture, thumbnailLink: picture.src }} />
                        <Button variant="contained" onClick={handleSubmit}>Add</Button>
                    </>
                }
            </Box>
            {step < 2 &&
                <div style={{ position:'fixed', bottom: '30px', right: '30px' }}>
                    <Button variant="contained" color="warning" onClick={() => setStep(step + 1)}>Next</Button>
                </div>
            }
        </>
    )
}