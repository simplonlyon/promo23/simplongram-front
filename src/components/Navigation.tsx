import { AuthContext } from "@/auth/auth-context";
import { AppBar, Toolbar, IconButton, Typography, Box, Button } from "@mui/material";
import Link from "next/link";
import { useContext } from "react";


export default function Navigation() {
    const { token, setToken } = useContext(AuthContext);

    return (
        <AppBar component="nav" position="relative">
            <Toolbar>
                <Typography
                    variant="h6"
                    component="div"
                    sx={{ flexGrow: 1 }}
                >
                    Simplongram
                </Typography>
                <Box>
                    <Link href="/" passHref>
                        <Button sx={{ color: '#fff' }}>
                            Home
                        </Button>
                    </Link>
                    <Link href="/add" passHref>

                        <Button sx={{ color: '#fff' }}>
                            Add
                        </Button>
                    </Link>
                    {token ?


                        <Button sx={{ color: '#fff' }} onClick={() => setToken(null)}>
                            Logout
                        </Button>
                        :

                        <Link href="/login" passHref>

                            <Button sx={{ color: '#fff' }}>
                                Login
                            </Button>
                        </Link>
                    }
                </Box>
            </Toolbar>
        </AppBar>
    )
}