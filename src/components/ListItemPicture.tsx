import { Picture } from "@/entities"
import { Card, CardMedia, CardContent, Typography, CardActions, Button } from "@mui/material";
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import { useContext } from "react";
import { AuthContext } from "@/auth/auth-context";
import { likePicture } from "@/picture-service";
import jwtDecode from "jwt-decode";

interface Props {
  picture: Picture;
  onLike?: (picture:Picture) => void;
}

export default function ListItemPicture({ picture, onLike = () => {} }: Props) {
  const { token } = useContext(AuthContext);

  function isLiked() {
    if (token) {
      const decoded = jwtDecode<any>(token);
      if (picture.likes?.find(item => item.email == decoded.username)) {
        return true;
      }
    }
    return false;
  }

  async function like() {
    const updated = await likePicture(picture);
    onLike(updated);
  }
console.log(picture);
  return (
    <Card>
      <CardMedia
        image={picture.thumbnailLink}
        title="The Picture"
        sx={{ height: 100 }}
      />
      <CardContent sx={{ height: 100 }}>
        <Typography variant="body2" color="text.secondary" >
          {picture.description?.substring(0, 150)}...
        </Typography>

        <Typography variant="body2" color="text.secondary">
          {picture.publicationDate?.toString()}
        </Typography>
      </CardContent>
      <CardActions>
        {token &&

          <Button size="small" onClick={like}>

            {isLiked() ? <FavoriteIcon /> :
              <FavoriteBorderIcon />
            }
            {picture.likes?.length}

          </Button>

        }
      </CardActions>
    </Card>
  )
}