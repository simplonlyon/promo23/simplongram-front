import { AuthContextProvider } from '@/auth/auth-context';
import Navigation from '@/components/Navigation';
import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import '../auth/axios-config';


export default function App({ Component, pageProps }: AppProps) {
  return (
    <AuthContextProvider>

      <Navigation />
      <Component {...pageProps} />
    </AuthContextProvider>
  );
}
