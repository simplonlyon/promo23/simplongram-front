import ListItemPicture from "@/components/ListItemPicture";
import { Picture } from "@/entities"
import { fetchAllPictures } from "@/picture-service";
import { Grid } from "@mui/material";
import { GetServerSideProps } from "next";
import { useState } from "react";

interface Props {
  pictures: Picture[];
}

export default function Home({ pictures:propPic }: Props) {
  const [pictures, setPictures]  = useState(propPic);

  function updatePicture(picture:Picture) {
    setPictures(pictures.map(item => item.id == picture.id ? picture:item));
  }

  return (
    <>
      <h1>All pictures</h1>
      <Grid container spacing={2}>
        {pictures.map(pict =>
          <Grid item xs={6} md={3} key={pict.id}>
            <ListItemPicture picture={pict} onLike={updatePicture} />
          </Grid>
        )}
      </Grid>

    </>
  )
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {


  return {
    props: {
      pictures: await fetchAllPictures()
    }
  }

}