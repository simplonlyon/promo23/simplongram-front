import { AuthContext } from "@/auth/auth-context";
import { postLogin, postRegister } from "@/auth/auth-service";
import { User } from "@/entities";
import { Button, TextField } from "@mui/material";
import { useRouter } from "next/router";
import { FormEvent, useContext, useState } from "react";



export default function login() {
    const router = useRouter();
    const {setToken} = useContext(AuthContext);

    const [isLogin, setIsLogin] = useState(true);
    const [repeatError, setRepeatError] = useState(false);

    const [user, setUser] = useState<User>({
        email: '',
        password: '',
        roles: []
    });

    function handleChange(event: any) {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        });
    }

    function handleRepeat(event: any) {

        if (event.target.value != user.password) {
            setRepeatError(true);
        } else {
            setRepeatError(false);
        }
    }
    /**
     * On récupère l'image dans l'input type file et on convertit celle ci
     * en base64 pour l'envoyer vers le back
     */
    function handleFile(event: any) {
        const reader = new FileReader();
        reader.onload = () => setUser({
            ...user,
            profilePicture: String(reader.result)
        });

        reader.readAsDataURL(event.target.files[0]);
    }

    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        if (isLogin) {
            const data = await postLogin(user.email, user.password!);
            setToken(data.token);
        } else {

            const data = await postRegister(user);
            setToken(data.token);

        }
        
        router.back();
    }
    return (
        <>
            <h1>Authentication</h1>
            <form onSubmit={handleSubmit}>

                <TextField label="Email" name="email" type="email" variant="outlined" onChange={handleChange} />
                <TextField label="Password" name="password" type="password" variant="outlined" onChange={handleChange} />

                {!isLogin &&
                    <>
                        <TextField label="Repeat password" type="password" variant="outlined" error={repeatError} onChange={handleRepeat} />
                        <Button
                            variant="contained"
                            component="label"
                        >
                            Upload Profile Picture
                            <input
                                type="file"
                                hidden
                                name="src"
                                onChange={handleFile}
                            />
                        </Button>

                    </>

                }
                <Button type="submit" variant="contained">Submit</Button>
                <Button variant="contained" onClick={() => setIsLogin(!isLogin)}>{isLogin? 'Register':'Login'}</Button>
            </form>
        </>
    )
}