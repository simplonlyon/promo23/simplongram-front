import { AuthContext, AuthContextProvider } from "@/auth/auth-context"
import Navigation from "@/components/Navigation"
import { fireEvent, getByRole, render, screen } from "@testing-library/react"
import { setCookie } from "nookies";




test('Navigation with token', () => {
    render(<AuthContext.Provider value={{ token: 'bloup', setToken: () => { } }} >
        <Navigation />
    </AuthContext.Provider>);

    expect(screen.queryByText('Login')).toBeNull();

    expect(screen.getByText('Logout')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: /Logout/ })).toBeInTheDocument();
})



test('Navigation without token', () => {
    render(<AuthContext.Provider value={{ token: null, setToken: () => { } }} >
        <Navigation />
    </AuthContext.Provider>);

    expect(screen.queryByText('Logout')).toBeNull();

    expect(screen.getByText('Login')).toBeInTheDocument();
})


test('Logout click should disconnect user', () => {
    setCookie(null, 'token', 'test-token');

    render(<AuthContextProvider>
        <Navigation />
    </AuthContextProvider>);
    
    const btnLogout = screen.getByText('Logout');
    expect(btnLogout).toBeInTheDocument();
    
    fireEvent.click(btnLogout);
    expect(screen.getByText('Login')).toBeInTheDocument();
})