import FormPicture from "@/components/FormPicture"
import { fireEvent, render, screen } from "@testing-library/react"
import userEvent from '@testing-library/user-event';

jest.mock('next/router', () => ({
    useRouter: jest.fn()
}))




test('FormPicture with token', () => {
    const file = new File(['hello'], 'hello.png', {type: 'image/png'})

    render(<FormPicture />);
    const btnNext = screen.getByText('Next');
    expect(btnNext).toBeInTheDocument();
    const uploadButton = screen.getByText(/upload/i);
    expect(uploadButton).toBeInTheDocument();
    
    userEvent.upload(uploadButton, file);

    fireEvent.click(btnNext);
    expect(screen.getByLabelText(/description/i)).toBeInTheDocument();

    fireEvent.click(btnNext);
    expect(screen.getByTitle('The Picture'))
})