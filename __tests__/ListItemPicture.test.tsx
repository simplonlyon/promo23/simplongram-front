import ListItemPicture from '@/components/ListItemPicture'
import { Picture } from '@/entities';
import {screen, render, getByTitle} from '@testing-library/react'

const testPicture:Picture = {
    src: 'testsrc',
    description: 'here is a long description for the picture',
    link: 'testsrc',
    thumbnailLink: 'testsrc',
    id: 1,
    publicationDate: '2023-03-23'
}


test('ListItemPicture should render', () => {

    render(<ListItemPicture picture={testPicture} />);

    const description = screen.getByText(/here is a long description/);
    expect(description).toBeInTheDocument();

    expect(screen.getByTitle('The Picture')).toBeInTheDocument();
})